/**
 * Ejercicio #9
 * Leer tres numeros enteros y determinar si el último digito. de los tres numeros es igual. 
 */
const lastDigitEquals = (num1, num2, num3) => {
  const num_arr = [num1, num2, num3]
  const last_arr = [ ...num_arr.map(n => Number(n).toString().split('')[ Number(n).toString().split('').length-1 ] ) ]
  
  return last_arr[0] == last_arr[1] && last_arr[2] == last_arr[0]
}

const startLastDigitsEquals = (num1, num2, num3) => {
  return lastDigitEquals(num1, num2, num3) 
  ? 'Los ultimos digitos son iguales.' 
  : 'Los ultimos digitos NO son iguales.'
}

// console.log(startLastDigitsEquals(43, 4545643, 56756853))

module.exports = { lastDigitEquals, startLastDigitsEquals }