/**
 * Ejercicio #13
 * Generar todas las tablas de multiplicar del 1 al 10
 */

const getTable = num => {
  let table = ""
  for (let i = 1; i <=10; i++){
    table += `${num} x ${i} = ${num*i} \n` 
  }

  return table
}

const getTableTo = num => {
  let tables = ""
  for (let i = 1; i <=num; i++){
    tables += getTable(i) 
  }
  return tables
}

// Uncoment this line to print the table
// console.log(getTableTo(10))


module.exports = { getTable, getTableTo }