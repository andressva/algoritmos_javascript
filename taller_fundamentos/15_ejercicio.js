/**
 * Ejercicio #15
 * Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se encuentra el número mayor. 
 */

const getGreater = nums => {
  let greater = 0
  nums.forEach((n, i) => {
    greater = n>greater || i==0 ? n : greater
  })
  return greater
}

arr = [56,23,78,45,45,12,3,67,2,4]
// console.log(getGreater(arr))

module.exports = { getGreater }