/**
 * Ejercicio #16
 * Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se 
 * encuentran los números terminados en O 
 */

 const getZerosIndex = nums => {
  return nums.map((n, i) => Number(n.toString().split('')[n.toString().split('').length - 1]) == 0 ? i : false ).filter(i => i)
}

arr = [56,23,70,45,45,12,30,60,2,4]
// console.log(getZerosIndex(arr))

module.exports = { getZerosIndex }