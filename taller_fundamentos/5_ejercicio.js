/**
 * Ejercicio #5
 * Leer un número entena de dos digitos y determinar si es primo y además si es negativo.
 */

const { calculatePrimo } =  require('./4_ejercicio.js')
const { isNegative } = require('./1_ejercicio.js')


const num = 23
console.log( calculatePrimo(num) )
console.log(isNegative(num))