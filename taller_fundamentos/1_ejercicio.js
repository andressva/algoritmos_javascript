/**
 * Ejercicio #1
 * Leer un número entero y determinar si es negativo.
 */

const isNegative = num => {
  if(num < 0) return `El numero ${num} es negativo`
  else return `El numero ${num} NO es negativo`
}

const play = () => {
  const num = 45
  console.log(isNegative(num))
}

// play()

module.exports = { isNegative }