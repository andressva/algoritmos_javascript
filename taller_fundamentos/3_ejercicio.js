/**
 * Ejercicio #3
 * Leer un número entena de dos digitos y determinar si ambos digitos son pares.
 */

 const theyArePairs = num => {
  const arr_nums = Number(num).toString().split('')
  const num1 = arr_nums[0]
  const num2 = arr_nums[1]
  if(num1%2 === 0 && num2%2 === 0) return "Los numeros son PARES"
  return "Al menos uno de ellos NO es PAR"
}


// console.log(theyArePairs(22) )

module.exports = { theyArePairs }
