/**
 * Ejercicio #8
 * Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 
 */

const { numEquals } = require('./7_ejercicio.js');

const isEqualSecondToPen = (num) => {
  const num_arr = Number(num).toString().split('')
  return numEquals(num_arr[1], num_arr[num_arr.length - 2]) 
  ? 'Los digitos son iguales.' 
  : 'Los digitos no son iguales'
}

// console.log(isEqualSecondToPen(2334))

module.exports = { isEqualSecondToPen }