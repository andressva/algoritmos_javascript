/**
 * Ejercicio #7
 * Leer un número entero de dos digitos y determinar si los dos digitos son iguales.
 */

const numEquals = (n1, n2) => {
  return n1 === n2
}

const theyAreEquals = (num) => {
  const num_arr = Number(num).toString().split('')
  return numEquals(num_arr[0], num_arr[1]) 
  ? 'Los digitos son iguales.' 
  : 'Los digitos no son iguales'
}

// console.log(theyAreEquals(55))


module.exports = { numEquals, theyAreEquals }