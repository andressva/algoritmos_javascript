/**
 * Ejercicio #4
 * Leer un número entena de dos digitos menor gue 20 y determinar si es primo.
 */

const isPrimo = (num, n=2) => {
  if(n >= num){
    return true
  } else if(num % n != 0){
    return isPrimo(num, n + 1)
  } else{
    return false
  }
}

const calculatePrimo = (num) => {
  if( isPrimo(num) ){
    return "El numero es PRIMO"
  }

  return "El numero NO es PRIMO"
}

// console.log(calculatePrimo(43))

module.exports = { calculatePrimo }
