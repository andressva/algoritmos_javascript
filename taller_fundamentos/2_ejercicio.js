/**
 * Ejercicio #2
 * Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.
 */

 const getSumOfDigits = num => {
  const arr_nums = Number(Math.abs(num)).toString().split('').map(n => Number(n))
  let sum = 0

  for (let n of arr_nums) {
    sum += n;
  }

  return sum
}

const play = () => {
  const num = -34
  return `La suma de los digitos de ${num} es: ${getSumOfDigits(num)}`
}

// console.log( play() )

module.exports = { getSumOfDigits }