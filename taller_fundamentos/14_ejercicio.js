/**
 * Ejercicio #14
 * Leer un número entero y mostrar en pantalla su tabla de multiplicar
 */


const { getTable } = require('./13_ejercicio')

const num = 21
console.log( getTable(num) )