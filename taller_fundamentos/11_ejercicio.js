/**
 * Ejercicio #11
 * Leer un número entero y determinar a cuánto es igual la suma de sus digitos.
 */
const {getSumOfDigits} = require('./2_ejercicio')

const num = 343647
console.log(getSumOfDigits(num))