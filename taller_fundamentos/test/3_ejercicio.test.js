const {theyArePairs} = require('../3_ejercicio')

test('Determinar si son pares 43', () => {
    expect(theyArePairs(43)).toBe("Al menos uno de ellos NO es PAR");
});

test('Determinar si son pares 82', () => {
    expect(theyArePairs(82)).toBe("Los numeros son PARES");
});