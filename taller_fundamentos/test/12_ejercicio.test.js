const { countOfOnes } = require('../12_ejercicio')

test('Test 124165321 return 3', () => {
    expect(countOfOnes(124165321)).toBe(3);
});


test('Test 100997109281112 return 5', () => {
    expect(countOfOnes(100997109281112)).toBe(5);
});

test('Test 9009970928 return 0', () => {
    expect(countOfOnes(9009970928)).toBe(0);
});