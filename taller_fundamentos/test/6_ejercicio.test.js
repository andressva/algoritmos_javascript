const {theyAreMultiples} = require('../6_ejercicio')

test('Test 9 es multiplo de 3', () => {
    expect(theyAreMultiples(93)).toBe("9 es multiplo de 3");
});

test('Test 5 no es multiplo de 4', () => {
    expect(theyAreMultiples(54)).toBe("Los digitos no son multilpos uno del otro.");
});