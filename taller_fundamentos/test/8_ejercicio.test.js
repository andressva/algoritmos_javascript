const {isEqualSecondToPen} = require('../8_ejercicio')

test('Test segundo igual al penultimo digito', () => {
    expect(isEqualSecondToPen(17345678)).toBe('Los digitos son iguales.' );
});
