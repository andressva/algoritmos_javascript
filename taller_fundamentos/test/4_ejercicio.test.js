const {calculatePrimo} = require('../4_ejercicio')

test('Determinar si 23 es primo ', () => {
    expect(calculatePrimo(23)).toBe("El numero es PRIMO");
});