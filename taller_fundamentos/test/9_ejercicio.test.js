const {startLastDigitsEquals} = require('../9_ejercicio')

test('Ultimos digitos iguales', () => {
    expect(startLastDigitsEquals(2345, 45765, 988765)).toBe('Los ultimos digitos son iguales.');
});

test('Ultimos digitos NO iguales', () => {
    expect(startLastDigitsEquals(2347, 45765, 988765)).toBe('Los ultimos digitos NO son iguales.');
});
