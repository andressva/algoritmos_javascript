const { getTable, getTableTo } = require('../13_ejercicio')

test('Table of 2', () => {
    expect(getTable(2)).toContain('2 x 10 = 20');
});


test('Tbale from 1 to 10', () => {
    expect(getTableTo(10)).toContain('1 x 10 = 10');
    expect(getTableTo(10)).toContain('3 x 8 = 24');
    expect(getTableTo(10)).toContain('10 x 8 = 80');
});