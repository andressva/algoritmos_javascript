const {getSumOfDigits} = require('../2_ejercicio')

test('Suma de los digitos de un numero entero', () => {
    expect(getSumOfDigits(58)).toBe(13);
});