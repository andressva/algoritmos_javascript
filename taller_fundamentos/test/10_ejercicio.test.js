const { difDivisor } = require('../10_ejercicio')

test('12 - 9 = 3; 3 es divisor de 9', () => {
    expect(difDivisor(12, 9)).toBe('La diferencia si es Divisor de alguno de los numeros' );
});


test('23 - 8 = 15; 15 no es divisor', () => {
    expect(difDivisor(23, 8)).toBe('La diferencia NO es Divisor de alguno de los numeros' );
});