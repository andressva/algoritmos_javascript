const {isNegative} = require('../1_ejercicio')

test('Numero negativo', () => {
    expect(isNegative(-23)).toBe(`El numero -23 es negativo`);
});