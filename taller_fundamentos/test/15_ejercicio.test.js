const { getGreater } = require('../15_ejercicio')

test('greater = 567', () => {
    expect(getGreater([2, 34, 65 , 67, 3, 5, 67, 89, 3, 4, 345, 567])).toBe(567);
});