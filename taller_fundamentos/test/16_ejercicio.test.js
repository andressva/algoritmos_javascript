const { getZerosIndex } = require('../16_ejercicio')

test('index [3, 5, 9]', () => {
    expect(getZerosIndex([2, 34, 65 , 60, 3, 50, 67, 89, 3, 40, 345, 57])).toEqual([3, 5, 9]);
});

test('index []', () => {
  expect(getZerosIndex([2, 34, 65 , 6, 3, 5, 67, 45, 57])).toEqual([]);
});