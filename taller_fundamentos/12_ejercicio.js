/**
 * Ejercicio #12
 * Leer un número entero y determinar cuantas veces tiene el digito 1
 */

const countOfOnes = num => {
  const arr_nums = Number(Math.abs(num)).toString().split('').map(n => Number(n))
  const one = 1
  
  const ones_arr = arr_nums.filter(n => n == one)
  return ones_arr.length
}

// console.log( countOfOnes(2314321) )

module.exports = { countOfOnes }