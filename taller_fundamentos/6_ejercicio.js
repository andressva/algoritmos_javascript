/**
 * Ejercicio #6
 * Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.
 */

const isMultiple = (num1, num2) => {
  const div = num1 / num2
  const result = (div%Math.trunc(div)) == 0

  return result
}

const theyAreMultiples = num => {
  const arr_nums = Number(num).toString().split('')
  if(isMultiple(arr_nums[0], arr_nums[1])) return `${arr_nums[0]} es multiplo de ${arr_nums[1]}`
  if(isMultiple(arr_nums[1], arr_nums[0])) return `${arr_nums[1]} es multiplo de ${arr_nums[0]}`
  return "Los digitos no son multilpos uno del otro."
}

// console.log(play(93))

module.exports = { theyAreMultiples }

