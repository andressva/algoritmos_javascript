/**
 * Ejercicio #10
 * Leer dos numeres enteros y determinar si la diferencia entre los dos es un número divisor exacto de alguno de los dos números.
 */
const isDivisor = (num1, num2) => {
  const div = num2 / num1
  const result = (div%Math.trunc(div)) == 0

  return result
}

const difDivisor = (num1, num2) => {
  const diff = num1 - num2
  return isDivisor(diff, num1) || isDivisor(diff, num2)
  ? 'La diferencia si es Divisor de alguno de los numeros'
  : 'La diferencia NO es Divisor de alguno de los numeros'
}

// console.log( difDivisor(12, 9) ) 
module.exports = { difDivisor }