const fs = require('fs')

const getDiff = (arr1, arr2) => {
    const arrOne = Object.keys(arr1)
    const arrTwo = Object.keys(arr2)

    const arrDiff = arrOne.filter(p => !arrTwo.includes(p)).concat(
        arrTwo.filter(pck => !arrOne.includes(pck))
    )

    return arrDiff
}

const getDiffPackages = () => {
    let packageOneFile = fs.readFileSync('./package1.json')
    let packageTwoFile = fs.readFileSync('./package2.json')

    const packageOne = JSON.parse(packageOneFile)
    const { dependencies: packageTwo } = JSON.parse(packageTwoFile)

    return getDiff( packageOne, packageTwo )
}

// console.log(getDiffPackages())

module.exports = { getDiff, getDiffPackages }