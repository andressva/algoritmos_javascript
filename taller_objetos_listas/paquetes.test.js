const { getDiff, getDiffPackages } = require('./paquetes.js')

test("{a: 12, b: 12, a: 12.4}, {c: 12, x: 3, b: 12.4}  return ['a', 'c', 'x']", () => {
    expect(getDiff(
        {a: 12, b: 12, a: 12.4}, 
        {c: 12, x: 3, b: 12.4}
    )).toEqual(['a', 'c', 'x']);
});

