const { all: pokemons } = require('poke-names')

const groupByAlphabetical = list => {
    const format = {}
    list.sort()
    
    list.forEach(item => {
        let firstLetter = item[0]
        if (format[firstLetter]){
            format[firstLetter].push(item)
        }else{
            format[firstLetter] = [item]
        }
    })

    return format
}

const displayGroupPokemons = obj => {
    let display = ''
    for(let key in obj ){
        display += `Cantidad de pokemones que inician con ${key}: ${obj[key].length}\n` 
    }
    return display
}

const displayListPokemons = obj => {
    let display = ''
    for(let key in obj ){
        display += `${key}\n`
        obj[key].forEach(poke => {
            display += `   ${poke}\n`
        })
    }
    return display
}

// console.log( displayGroupPokemons(groupByAlphabetical(pokemons)) )
console.log( displayListPokemons(groupByAlphabetical(pokemons)) )



